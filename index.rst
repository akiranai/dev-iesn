.. Zombie documentation master file, created by
   sphinx-quickstart on Sun Dec  1 22:15:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Zombie's documentation!
==================================

Modules MASTER
==============

.. toctree::
    :maxdepth: 4
    
    project_master
    
Modules SLAVE
=============

.. toctree::
    :maxdepth: 4
    
    project_slave
    

