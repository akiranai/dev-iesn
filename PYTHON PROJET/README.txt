# Project_build.zip : contient toutes les données du projet, fichier latex rapport, doc généré par sphinx doc...
# Project_clean.zip : contient seulement le programme
# html_doc.zip      : contient la documentation du code généré avec sphinx mais sous forme de page internet

# projet_documentation.pdf  : Documentation du code généré avec sphinx mais en pdf
# projet_python_rapport.pdf : Rapport du projet

Si besoin d'accèder à notre git, merci de me le faire part.

