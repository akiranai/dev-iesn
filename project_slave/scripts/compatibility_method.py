﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (c) 2019 tout droit reservé pour la haute école IESN de namur pour le cours de  dévelopement
#  Auteur :  Anthony Tayar & Théo Detrembleur


def exception_open(file_path: str, arg: str, data):
    """
    Crée le fichier en gérant les exceptions
    :param data:
    :param file_path:
    :param arg:
    :return:
    """
    try:
        with open(file_path, arg) as file:
            if arg == 'r':
                lines = file.read().splitlines()
                return lines
            elif arg == 'w' or arg == 'a':
                print(data)
                if isinstance(data, list) and data != "":
                    for item in data:
                        file.write("{}\n".format(item))
                elif isinstance(data, dict) and data != "":
                    print("Cannot handle dict value, use json method instead")
                elif isinstance(data, int) and data != "":
                    print("nanithefuck")
                elif data == "":
                    print("null argument")
    except IOError:
        print("ERROR: Impossible d'effectuer des actions sur le fichier : {0}".format(IOError))




