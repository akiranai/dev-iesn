﻿#  Copyright (c) 2019 tout droit reservé pour la haute école IESN de namur pour le cours de  dévelopement
#  Autheur :  Anthony Tayar & Théo Detrembleur

import os  # https://docs.python.org/3/library/os.html
import platform  # https://docs.python.org/3/library/platform.html
import re
import socket  # https://docs.python.org/3/library/socket.html
import uuid  # https://docs.python.org/3/library/uuid.html


def get_os_name() -> str:
    """
    :return: le nom de l'OS
    """
    return os.name


def get_plat_sys() -> str:
    """
    :return: le type de systeme
    """
    return platform.system()


def get_plat_rel() -> str:
    """
    :return: la version de l'os
    """
    return platform.release()


def get_ip() -> str:
    """
    :return: l'adresse ip du reseau

    .. note:: Pour ce projet on ne prend pas l'adresse ip publique vu que tournée en interne

    """
    return socket.gethostbyname(socket.gethostname())


def get_mac() -> str:
    """https://www.geeksforgeeks.org/extracting-mac-address-using-python/"""
    return ':'.join(re.findall('..', '%012x' % uuid.getnode()))


def get_win_ver() -> list:
    """

    :return:  les infos de la platforme windows
    """
    return platform.win32_ver()


def get_mac_ver() -> list:
    """
    :return: les infos de la platforme macOS
    """
    return platform.mac_ver()


def get_unix_libc_ver() -> list:
    """
    :return: les infos de la platforme UNIX (linux)
    """
    return platform.libc_ver()


