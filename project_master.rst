.. toctree::
    :maxdepth: 4

ARGPARSE
--------

-f  Chemin de la base de donné
-fp  Chemin de la base de donnée
-p  Port serveur
-hn  Addresse du serveur

-st  Démarre le serveur

-sl  Demarre les logs
-spl  Arrête les logs
-gl  Get log
-dd  ddos un serveur

-gui  Afficher menu interactif

master module
-------------

.. automodule:: project_master.master
    :members:
    :undoc-members:
    :show-inheritance:

Module de classe
================

.. toctree::
   :maxdepth: 4

   project_master_classes

