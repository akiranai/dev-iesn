.. toctree::
    :maxdepth: 4
    
START MODULE
------------

.. automodule:: project_slave.start
    :members:
    :undoc-members:
    :show-inheritance:

SLAVE MODULE
------------
.. automodule:: project_slave.slave
    :members:
    :undoc-members:
    :show-inheritance:

SCRIPTS
=======

.. toctree::
   :maxdepth: 4

   project_slave_scripts