.. toctree::
   :maxdepth: 3

scripts.client\_infos module
----------------------------

.. automodule:: project_slave.scripts.client_infos
    :members:
    :undoc-members:
    :show-inheritance:

scripts.compatibility\_method module
------------------------------------

.. automodule:: project_slave.scripts.compatibility_method
    :members:
    :undoc-members:
    :show-inheritance:

