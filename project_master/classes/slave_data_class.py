#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (c) 2019 tout droit reservé pour la haute école IESN de namur pour le cours de  dévelopement
#  Autheur :  Anthony Tayar & Théo Detrembleur

import json
import os


class SlaveData:
    """
    Module de gestion des esclaves (client) répertoriés dans une BDD en .json
    """

    def __init__(self, bdd_path: str,
                 os_name: str,
                 sys: str,
                 release: str,
                 ip_addr: str,
                 mac_addr: str,
                 ver: str):
        """
        :param bdd_path: Path BDD
        :param os_name: Nom de l'OS
        :param sys: Systeme de l'OS
        :param release: Numero release de l'OS
        :param ip_addr: Adresse IP de l'OS
        :param mac_addr: Adresse MAC de l'OS
        :param ver: Version de l'OS, infos complémentaire

        .. note:: Garder le chemin par défaut de la base de donnée

        """

        self.__id = 0
        self.__os_name = os_name
        self.__ip_addr = ip_addr
        self.__mac_addr = mac_addr
        self.__client_sys = sys
        self.__client_release = release
        self.__client_ver = ver
        self.__log_path = ""
        self.__bdd_path = bdd_path
        self.json_file_creator()
        self.def_id()

    def get_id(self) -> int:
        """
        :return: ID client
        :type: int
        """
        return self.__id

    def get_os_name(self) -> str:
        """
        :return: nom client
        :type: str
        """
        return self.__os_name

    def get_ip(self) -> str:
        """
        :return: Client IP
        :type: str
        """
        return self.__ip_addr

    def get_mac(self) -> str:
        """
        :return: MAC adresse client
        :type: str
        """
        return self.__mac_addr

    def get_bdd_path(self) -> str:
        """
        :return: BDD file path
        :type: str
        """
        return self.__bdd_path

    def get_log_path(self) -> str:
        """
        :return: BDD file path
        :type: str
        """
        return self.__log_path

    def set_id(self, client_id):
        """Defini l'ID client

        .. note:: A eviter de changer manuellement, methode pose la en cas de debug ou pour init

        """
        self.__id = client_id

    def set_name(self, client_name):
        """set client name"""
        self.__os_name = client_name

    def set_ip(self, client_ip: str):
        """set client IP"""
        self.__ip_addr = client_ip

    def set_bdd_path(self, bdd_path: str):
        """set BDD file path """
        self.__bdd_path = bdd_path

    def to_string(self) -> str:
        """
        :return: formatted string
        :type: str
        """
        formatted_string = ("{} : {} = {}".format(self.get_id(), self.get_os_name(), self.get_ip()))
        return formatted_string

    def log_file_gen(self):
        """Va généré le fichier log si il n'existe pas encore sinon écrit par dessus"""
        if self.__log_path == "":  # si path pas défini
            print("Creation log")
            self.__log_path = ("slave_data/%s-%s.txt" % (self.get_id(), self.get_os_name()))
            file = open(self.__log_path, 'w')
            file.close()
        else:
            print("Log file opening")
            file = open(self.__log_path, 'w')
            file.close()

    def get_log_file(self) -> str:
        """
        :return: log file
        :type: str
        """
        if self.get_log_path() == "":
            print("Ce fichier log n'existe pas")
            pass
        else:
            print("Return log file")
            return self.get_log_path()

    def def_id(self):
        """Génère l'ID du client dans la BDD
        """
        try:
            id_value = 0
            with open(self.get_bdd_path(), 'r') as bdd_file:
                bdd_data = json.load(bdd_file)
                for data in bdd_data['client']:
                    if 'id' not in data:
                        self.set_id(id_value)
                    else:
                        id_value += 1
        except IOError:
            print(IOError)
        except json.JSONDecodeError as json_err:
            print(json_err)
            pass

    def json_file_creator(self):
        """Creation de la bdd en json si n'existe pas
        """
        # Empty data for init
        formatted_json = {'client': []}
        try:
            if not os.path.exists(self.get_bdd_path()):
                with open(self.get_bdd_path(), 'w') as bdd_file:
                    json.dump(formatted_json, bdd_file)
        except OSError as error:
            print(error)

    def destroy(self):
        """Va se supprimer de la BDD
        """
        try:
            file = open(self.get_bdd_path(), 'r')
            old_dt = json.load(file)
            file.close()
            for data in range(len(old_dt)):
                if old_dt['client'][data]['id'] == self.get_id():
                    old_dt['client'].pop(data)
            with open(self.get_bdd_path(), 'w') as bf:
                json.dump(old_dt, bf)
        except IOError:
            print(IOError)

    def to_json(self):
        """Module convertissant les données du client sous forme de base de donnée en JSON
        """
        formatted_json = {
                'id': self.__id,
                'name': self.__os_name,
                'log': self.__log_path,
                'ip': self.__ip_addr,
                'mac': self.__mac_addr,
                'sys': self.__client_sys,
                'release': self.__client_release,
                'ver': self.__client_ver
            }
        condition = False
        try:
            file = open(self.get_bdd_path(), 'r')
            old_dt = json.load(file)
            file.close()
            i = 0
            for data in old_dt['client']:
                if str(self.get_ip()) in data['ip']:
                    print("Donnée existant dans la BDD, MAJ")
                    condition = True
                    old_dt['client'][i] = formatted_json
                    break
                i += 1

            with open(self.get_bdd_path(), 'w') as bf:
                # Si la ligne existe, la maj sinon l'ajouter
                if condition:
                    json.dump(old_dt, bf)
                else:
                    old_dt['client'].append(formatted_json)
                    json.dump(old_dt, bf)
            self.log_file_gen()
        except IOError:
            print(IOError)
            pass
        except json.JSONDecodeError as json_err:
            print(json_err)
            pass
