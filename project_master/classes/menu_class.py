#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (c) 2019 tout droit reservé pour la haute école IESN de namur pour le cours de  dévelopement
#  Autheur :  Anthony Tayar & Théo Detrembleur
# doc http://docutils.sourceforge.net/docs/user/rst/quickref.html
import curses


class Menu:
    """Class permettant de créer des menus facilement avec curse selon une syntaxe d'affichage

    - [1] un menu
    - [2] un autre menu

    ::

        menu1 = Menu("Nom du menu", 1, 2, window)
        menu1.menu_add(1, "Choix 1", unemethod(), win1, 2)
        menu1.menu_add(2, "Choix 2", unemethod2(), win1, 2)
        menu1.text_title = "Menu 1"

    .. note:: Par defaut q quitte le menu
    """

    def __init__(self, menu_name: str, pos_x: int, pos_y: int, window_attr: curses):
        """
        :param menu_name: Nom du menu
        :param pos_x: position colonne X
        :param pos_y: position colonne y
        :param window_attr: Nom de la fenetre curse
        """
        self.menu = {}
        self.menu_name = menu_name
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.window_attr = window_attr
        self.custom_pos_x = 0
        self.custom_pos_y = 0
        self.menu_pause = False

    def menu_add(self, nb_menu: int, name_menu: str, arg_menu, window: curses, color1: int, color2: int, TriggersExit):
        """
        :param nb_menu: numero du menu
        :param name_menu: nom du menu
        :param arg_menu: argument pour le menu
        :param window: argument curse window
        :param color: 1 NOIR 2 BLEU 3 CYAN 4 VERT 5 MAGENTA 6 ROUGE 7 BLANC 8 JAUNE
        :param TriggersExit:
        """
        self.menu[nb_menu] = [name_menu, arg_menu, window, color1, color2, TriggersExit]
        window.refresh()

    def pause(self, arg: bool):
        self.menu_pause = arg

    def size(self):
        """
        ruturn: la taille du menu
        """
        return len(self.menu)

    def display(self):
        """
        Affiche le menu
        """
        self.window_attr.erase()
        self.window_attr.box()
        self.window_attr.addstr(self.custom_pos_x, self.custom_pos_y, self.menu_name, self.color(7))
        for x in range(1, self.size() + 1):
            self.menu[x][2].addstr(self.pos_x + x, self.pos_y, ("[{}] ".format(x)), self.color(self.menu[x][3]))
            self.menu[x][2].addstr(self.pos_x + x, self.pos_y + 4, self.menu[x][0], self.color(self.menu[x][4]))
        self.window_attr.refresh()

    def custom_pos_title(self, pos_x: int, pos_y: int):
        """Change la position du titre

        :param pos_x: Col X
        :param pos_y: Col Y
        """
        self.custom_pos_x = pos_x
        self.custom_pos_y = pos_y

    def text_title(self, text_title: str):
        """Change le texte titre

        :param text_title: Nouveau titre
        :return:
        """
        self.menu_name = text_title

    def color(self, value: int):
        """
        Voir : https://docs.python.org/3/library/curses.html#constants
        ..warning::
        ne fonctionne pas correctement sous windows
        :param value: valeur pour la couleur
        :return:
        """
        if value == 1:
            return curses.COLOR_BLACK
        elif value == 2:
            return curses.COLOR_BLUE
        elif value == 3:
            return curses.COLOR_CYAN
        elif value == 4:
            return curses.COLOR_GREEN
        elif value == 5:
            return curses.COLOR_MAGENTA
        elif value == 6:
            return curses.COLOR_RED
        elif value == 7:
            return curses.COLOR_WHITE
        elif value == 8:
            return curses.COLOR_YELLOW

    def get_name(self) -> str:
        """
        :return: Return le nom du menu
        """
        return self.menu_name

    def set_name(self, new_name: str):
        """
        :param new_name: Change le nom du menu
        """
        self.menu_name = new_name

    def run(self):
        """Crée le menu et l'affiche

        * Dans une boucle while, attend une entrée de l'utilisateur
        * Si la touche entrée est q ou autre prédéfini, casse la boucle
        * Sinon la fonction va chercher si existe ou pas un élément correspondant
        * Si c'est une fonction, l'appel, si sous menu, passe à ce sous menu
        """
        self.display()
        while True:
            if self.menu_pause is False:
                user_input = self.window_attr.getch()
                # Par defaut pour curses, si nodelay(True) la fonctionne retourne constament -1
                if user_input != -1:
                    # Si q ou numero du menu pour quitter
                    try:
                        if user_input == ord('q') or self.menu[int(chr(user_input))][5]:
                            break
                        # Pour chaque élément se trouvant dans le menu
                        for x in range(0, self.size()):
                            # Si c'est une fonction, l'appler
                            if x == int(chr(user_input)):
                                if callable(self.menu[int(chr(user_input))][1]):
                                    self.menu[int(chr(user_input))][1]()
                                    self.display()
                                # Si il y a un sous menu, l'afficher
                                else:
                                    self.menu[int(chr(user_input))][1].run()
                                    self.display()
                        else:
                            continue
                    except:
                        continue
