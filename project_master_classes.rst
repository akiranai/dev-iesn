.. toctree::
   :maxdepth: 3

classes.menu\_class module
--------------------------

.. automodule:: project_master.classes.menu_class
    :members:
    :undoc-members:
    :show-inheritance:

classes.slave\_data\_class module
---------------------------------

.. automodule:: project_master.classes.slave_data_class
    :members:
    :undoc-members:
    :show-inheritance:

